- [Personal Info](#org6e857bf)
- [General](#org02d0a65)
  - [Are we running on a GUI Emacs?](#org5408993)
  - [Are you a ROOT user?](#orgc5a36ca)
- [Appearance](#org02aa68d)
  - [DOOM THEME](#orgf369fc8)
  - [FONTS](#orgc4360f6)
- [Bindings](#orgd699fcc)
- [Packages](#org556d08d)
  - [ORG MODE](#org09dda74)
  - [Ox-gfm](#orgaa894fb)
- [Modeline](#org87deb96)



<a id="org6e857bf"></a>

# Personal Info

Some functionality uses this to identify you, e.g. GPG configuration, email clients, file templates and snippets.

```emacs-lisp
(setq user-full-name "Michael Pappe"
      user-mail-address "nokogiri@gefjon.org")
```


<a id="org02d0a65"></a>

# General


<a id="org5408993"></a>

## Are we running on a GUI Emacs?

```emacs-lisp
(defconst *sys/gui*
  (display-graphic-p))
```


<a id="orgc5a36ca"></a>

## Are you a ROOT user?

```emacs-lisp
(defconst *sys/root*
  (string-equal "root" (getenv "USER")))
```


<a id="org02aa68d"></a>

# Appearance


<a id="orgf369fc8"></a>

## DOOM THEME

Setting the theme to doom-one. To try out new themes, I set a keybinding for counsel-load-theme with &rsquo;SPC h t&rsquo;.

```emacs-lisp
(setq doom-theme 'doom-gruvbox)
(map! :leader
      :desc "change theme"
      "h t " #'counsel-load-theme )
```


<a id="orgc4360f6"></a>

## FONTS

Settings related to fonts within Doom Emacs:

-   &rsquo;doom-font&rsquo; &#x2013; standard monospace font that is used for most things in Emacs.
-   &rsquo;doom-variable-pitch-font&rsquo; &#x2013; variable font which is useful in some Emacs plugins.
-   &rsquo;doom-big-font&rsquo; &#x2013; used in doom-big-font-mode; useful for presentations.
-   &rsquo;font-lock-comment-face&rsquo; &#x2013; for comments.
-   &rsquo;font-lock-keyword-face&rsquo; &#x2013; for keywords with special significance, like ‘for’ and ‘if’ in C.

```emacs-lisp
(setq doom-font (font-spec :family "MesloLGLDZ Nerd Font Mono" :size 12)
      doom-variable-pitch-font (font-spec :family "MesloLGLDZ Nerd Font" :size 12)
      doom-big-font (font-spec :family "MesloLGLDZ Nerd Font" :size 20))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
```


<a id="orgd699fcc"></a>

# Bindings


<a id="org556d08d"></a>

# Packages


<a id="org09dda74"></a>

## ORG MODE

Note that I wrapped most of this in (after! org). Without this, my settings might be evaluated too early, which will result in my settings being overwritten by Doom&rsquo;s defaults. I have also enabled org-journal by adding (+journal) to the org section of my Doom Emacs init.el.

```emacs-lisp
(after! org
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
  (setq org-directory "~/Org/"
        org-agenda-files '("~/Org/agenda.org")
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-log-done 'time
        org-journal-dir "~/Org/journal/"
        org-journal-date-format "%B %d, %Y (%A) "
        org-journal-file-format "%Y-%m-%d.org"
        org-hide-emphasis-markers t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("google" . "http://www.google.com/search?q=")
            ("arch-wiki" . "https://wiki.archlinux.org/index.php/")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             "TODO(t)"           ; A task that is ready to be tackled
             "BLOG(b)"           ; Blog writing assignments
             "PROJ(p)"           ; A project that contains other tasks
             "WAIT(w)"           ; Something is holding up this task
             "|"                 ; The pipe necessary to separate "active" states and "inactive" states
             "DONE(d)"           ; Task has been completed
             "CANCELLED(c)" )))) ; Task has been cancelled

```

I was tired of having to run org-babel-tangle after saving my literate dotfiles. So the following function runs org-babel-tangle upon saving any org-mode buffer. This is asynchronous meaning that it dispatches the tangle function to a subprocess, so that the main Emacs is not blocked while it runs.

```emacs-lisp
(defun dt/org-babel-tangle-async (file)
  "Invoke `org-babel-tangle-file' asynchronously."
  (message "Tangling %s..." (buffer-file-name))
  (async-start
   (let ((args (list file)))
  `(lambda ()
        (require 'org)
        ;;(load "~/.emacs.d/init.el")
        (let ((start-time (current-time)))
          (apply #'org-babel-tangle-file ',args)
          (format "%.2f" (float-time (time-since start-time))))))
   (let ((message-string (format "Tangling %S completed after " file)))
     `(lambda (tangle-time)
        (message (concat ,message-string
                         (format "%s seconds" tangle-time)))))))

(defun dt/org-babel-tangle-current-buffer-async ()
  "Tangle current buffer asynchronously."
  (dt/org-babel-tangle-async (buffer-file-name)))
```


<a id="orgaa894fb"></a>

## Ox-gfm

Github Flavored Markdown exporter for Org Mode

```emacs-lisp
(use-package ox-gfm
  :defer 3)
```


<a id="org87deb96"></a>

# Modeline

```emacs-lisp
;; Whether display icons in the mode-line.
;; While using the server mode in GUI, should set the value explicitly.
(setq doom-modeline-icon (display-graphic-p))

;; Whether display the icon for `major-mode'. It respects `doom-modeline-icon'.
(setq doom-modeline-major-mode-icon t)

;; Whether display the colorful icon for `major-mode'.
;; It respects `all-the-icons-color-icons'.
(setq doom-modeline-major-mode-color-icon t)

;; Whether display the icon for the buffer state. It respects `doom-modeline-icon'.
(setq doom-modeline-buffer-state-icon t)

;; Whether display the modification icon for the buffer.
;; It respects `doom-modeline-icon' and `doom-modeline-buffer-state-icon'.
(setq doom-modeline-buffer-modification-icon t)

;; Whether to use unicode as a fallback (instead of ASCII) when not using icons.
(setq doom-modeline-unicode-fallback nil)

;; Whether display the minor modes in the mode-line.
(setq doom-modeline-minor-modes nil)

```

```emacs-lisp
(setq dokuwiki-xml-rpc-url "https://feedme.fishoeder.net/lib/exe/xmlrpc.php")
(setq dokuwiki-login-user-name "nokogiri")
```
