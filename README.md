- [Info](#org566e50c)
- [Screenshots](#org83a01c3)
- [Used Software:](#org4f44540)



<a id="org566e50c"></a>

# Info

A collection of my accumulated dotfiles, right now really messy and anythig but finished. Feel free to use.


<a id="org83a01c3"></a>

# Screenshots

![img](.asset/desktop.png "Desktop empty") [Original Wallpaper](https://www.reddit.com/r/NintendoSwitch/comments/c1m9zc/the_legend_of_zelda_high_quality_4k_pc_wallpape)


<a id="org4f44540"></a>

# Used Software:

-   [sway](https://github.com/swaywm/sway) (wlroots based wayland compositor)
-   [swaybg](https://github.com/swaywm/swaybg) (wallpaper)
-   [mako](https://wayland.emersion.fr/mako/) (notifications)

```toml
[terminal]
# The VT to run the greeter on. Can be "next", "current" or a number
# designating the VT.
vt = 1

# The default session, also known as the greeter.
[default_session]
command = "sway --config /etc/greetd/sway-config"

# The user to run the command as. The privileges this user must have depends
# on the greeter. A graphical greeter may for example require the user to be
# in the `video` group.
user = "greeter"
```
