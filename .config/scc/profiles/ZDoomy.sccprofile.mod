{
    "_": "", 
    "buttons": {
        "A": {
            "action": "button(Keys.KEY_ENTER)", 
            "name": "Jump"
        }, 
        "B": {
            "action": "button(Keys.KEY_Q)", 
            "name": "Drop"
        }, 
        "BACK": {
            "action": "button(Keys.KEY_TAB)", 
            "name": "Info"
        }, 
        "C": {
            "action": "hold(menu('Default.menu'), menu('Default.menu'))"
        }, 
        "LB": {
            "action": "mouse(Rels.REL_WHEEL, -1)", 
            "name": "Previous Weapon"
        }, 
        "LGRIP": {
            "action": "button(Keys.KEY_LEFTSHIFT)", 
            "name": "Crouch"
        }, 
        "RB": {
            "action": "mouse(Rels.REL_WHEEL, 1)", 
            "name": "Fire"
        }, 
        "RGRIP": {
            "action": "button(Keys.KEY_ENTER)", 
            "name": "Enter"
        }, 
        "RPAD": {
            "action": "button(Keys.BTN_MIDDLE)"
        }, 
        "START": {
            "action": "button(Keys.KEY_ESC)", 
            "name": "Menu"
        }, 
        "STICKPRESS": {
            "action": "button(Keys.KEY_LEFTCTRL)", 
            "name": "Sprint"
        }, 
        "X": {
            "action": "button(Keys.KEY_E)", 
            "name": "Inventory"
        }, 
        "Y": {
            "action": "button(Keys.KEY_LEFTSHIFT)", 
            "name": "Chat"
        }
    }, 
    "cpad": {}, 
    "gyro": {}, 
    "is_template": false, 
    "menus": {}, 
    "pad_left": {
        "action": "click(dpad(button(Keys.KEY_F11), keyboard(), button(Keys.KEY_F), button(Keys.KEY_C)))"
    }, 
    "pad_right": {
        "action": "smooth(8, 0.78, 2.0, feedback(RIGHT, 256, sens(2.5, 2.5, ball(mouse()))))"
    }, 
    "stick": {
        "action": "deadzone(MINIMUM, 0, dpad(30, button(Keys.KEY_UP), button(Keys.KEY_DOWN), button(Keys.KEY_COMMA), button(Keys.KEY_DOT)))"
    }, 
    "trigger_left": {
        "action": "trigger(50, 255, button(Keys.BTN_RIGHT))"
    }, 
    "trigger_right": {
        "action": "trigger(50, 255, button(Keys.BTN_MOUSE))"
    }, 
    "version": 1.4
}