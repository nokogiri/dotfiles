{
    "_": "", 
    "buttons": {
        "A": {
            "action": "button(Keys.BTN_GAMEPAD)"
        }, 
        "B": {
            "action": "button(Keys.BTN_EAST)"
        }, 
        "BACK": {
            "action": "button(Keys.BTN_SELECT)"
        }, 
        "C": {
            "action": "hold(menu('Default.menu'), menu('Default.menu'))"
        }, 
        "LB": {
            "action": "button(Keys.BTN_TL)"
        }, 
        "LGRIP": {
            "action": "button(Keys.BTN_GAMEPAD)"
        }, 
        "RB": {
            "action": "button(Keys.BTN_TR)"
        }, 
        "RGRIP": {
            "action": "button(Keys.BTN_NORTH)"
        }, 
        "START": {
            "action": "button(Keys.BTN_START)"
        }, 
        "X": {
            "action": "button(Keys.BTN_NORTH)"
        }, 
        "Y": {
            "action": "button(Keys.BTN_WEST)"
        }
    }, 
    "cpad": {}, 
    "gyro": {
        "action": "feedback(LEFT, 1536, 8, XY(axis(Axes.ABS_X), axis(Axes.ABS_Y)))"
    }, 
    "is_template": false, 
    "menus": {}, 
    "pad_left": {
        "action": "dpad(hatup(Axes.ABS_HAT0Y), hatdown(Axes.ABS_HAT0Y), hatleft(Axes.ABS_HAT0X), hatright(Axes.ABS_HAT0X))"
    }, 
    "pad_right": {
        "action": "XY(axis(Axes.ABS_RX), axis(Axes.ABS_RY))"
    }, 
    "stick": {
        "action": "XY(axis(Axes.ABS_X), axis(Axes.ABS_Y))"
    }, 
    "trigger_left": {
        "action": "trigger(254, button(Keys.KEY_1)) and axis(Axes.ABS_Z)"
    }, 
    "trigger_right": {
        "action": "trigger(254, button(Keys.KEY_2)) and axis(Axes.ABS_RZ)"
    }, 
    "version": 1.4
}