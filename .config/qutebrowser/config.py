#!/bin/env python
import subprocess
import os
from qutebrowser.api import interceptor

config.load_autoconfig()

try:
    config.source('pyconfig/redirectors.py')

except ImportError:
    pass

try:
    config.source('pyconfig/gruvbox-dp.py')
except ImportError:
    pass


with config.pattern('chrome-devtools://*') as p:
    p.content.cookies.accept = 'all'

with config.pattern('chrome://*') as p:
    p.content.cookies.accept = 'all'


c.content.blocking.enabled = True
c.content.blocking.hosts.lists = ['https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts']
c.content.blocking.method = 'both'
c.content.blocking.adblock.lists = [
        'https://easylist.to/easylist/easylist.txt',
        'https://easylist.to/easylist/easyprivacy.txt',
        'https://easylist.to/easylist/fanboy-annoyance.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/annoyances.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/badware.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters-2020.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters-2021.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/privacy.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/resource-abuse.txt',
        'https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/unbreak.txt',
        'https://www.malwaredomainlist.com/hostslist/hosts.txt',
        'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=adblockplus&showintro=1&mimetype=plaintext',
        'https://secure.fanboy.co.nz/fanboy-cookiemonster.txt',
        'https://raw.githubusercontent.com/abp-filters/abp-filters-anti-cv/master/english.txt',
        'https://easylist.to/easylistgermany/easylistgermany.txt'
        ]

c.colors.webpage.preferred_color_scheme = 'dark'

c.colors.webpage.darkmode.enabled = False
c.colors.webpage.darkmode.algorithm = 'lightness-cielab'
c.colors.webpage.darkmode.threshold.text = 150
c.colors.webpage.darkmode.threshold.background = 205
c.colors.webpage.darkmode.policy.images = 'smart'
c.fonts.default_family = 'MesloLGS Nerd Font'
c.fonts.web.family.standard = c.fonts.default_family
c.fonts.web.family.serif = c.fonts.default_family
c.fonts.web.family.sans_serif = c.fonts.default_family
c.fonts.web.family.fixed = 'FiraCode Nerd Font'

# Bindings for normal mode
config.bind('zl', 'spawn --userscript qute-pass ')
config.bind('zol', 'spawn --userscript qute-pass --otp-only')
config.bind('zpl', 'spawn --userscript qute-pass --password-only')
config.bind('zul', 'spawn --userscript qute-pass --username-only')
config.bind("v", 'hint links spawn funnel "{hint-url}"')

config.bind(',v', 'hint links spawn ~/.local/bin/umpv "{hint-url}"')
config.bind(',a', 'hint links spawn ~/.local/bin/umpa "{hint-url}"')

config.bind('<Alt+Left>', 'back')
config.bind('<Alt+Right>', 'forward')
c.fileselect.handler = 'external'
c.fileselect.multiple_files.command = ["kitty", "ranger", "--choosefiles={}"]
c.fileselect.single_file.command = ["kitty", "ranger", "--choosefile={}"]

c.scrolling.smooth = True

c.content.pdfjs = True
c.downloads.remove_finished = 3000


css = '~/.local/share/qutebrowser/gruvbox-all-sites.css'
config.bind(',n', f'config-cycle content.user_stylesheets {css} ""')

