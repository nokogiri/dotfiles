syntax enable

" Filetype detection
filetype plugin on

" Format indentation
if has("autocmd")
  filetype plugin indent on
endif

" add some tags for html indentations
:let g:html_indent_inctags = "html,body,head,tbody"

set smartindent
set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4
set showcmd			" Show (partial) command in status line.
set showmode        " Show indication of having changed modes
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set hlsearch        " Highlight search
"nnoremap <silent> <C-l> :nohlsearch<CR><C-l>        " set C-l to :nohlsearch
set autowrite		" Automatically save before commands like :next and :make
set hidden          " Hide buffers when they are abandoned
set mouse=a			" Enable mouse usage (all modes)
"set linebreak		" Line wrap
set tw=0           " Lines longer than 79 chars will be wrapped
"set colorcolumn=+1  " Colors the tw+1 column
set number	        " Line numbering
au TermOpen * setlocal listchars= nonumber norelativenumber
set cursorline
set modeline

" airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#wordcount#filetypes = 'pandoc\|text\|' "Add support when pandoc is activated
let g:airline_theme='minimalist'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let base16colorspace=256  " Access colors present in 256 colorspace

let g:bufferline_echo = 0
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.config/nvim/plugged')
Plug 'editorconfig/editorconfig-vim'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'mhinz/vim-startify'
Plug 'cespare/vim-toml'
Plug 'chriskempson/base16-vim'
Plug 'voldikss/vim-floaterm'
Plug 'ryanoasis/vim-devicons'
Plug 'baskerville/vim-sxhkdrc'
Plug 'mboughaba/i3config.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'pangloss/vim-javascript'
Plug 'digitaltoad/vim-pug'
"Plug 'francoiscabrol/ranger.vim'
"Plug 'rbgrouleff/bclose.vim'
call plug#end()

colorscheme base16-onedark

nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
let NERDTreeShowHidden=1

nnoremap <C-l> :ls<CR>
nnoremap <silent> <C-f> :bn<CR>
nnoremap <silent> <C-b> :bp<CR>

" Register ccls C++ lanuage server.
if executable('ccls')
   au User lsp_setup call lsp#register_server({
      \ 'name': 'ccls',
      \ 'cmd': {server_info->['ccls']},
      \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'compile_commands.json'))},
      \ 'initialization_options': {'cache': {'directory': '/tmp/ccls/cache' }},
      \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'cc'],
      \ })
endif

if has("autocmd")
  augroup templates
    autocmd BufNewFile *.html 0r ~/.config/nvim/skel/skeleton.html
  augroup END
endif

aug i3config_ft_detection
  au!
  au BufNewFile,BufRead ~/.config/i3/config set filetype=i3config
aug end
