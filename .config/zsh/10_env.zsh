# aur settings
export AUR_PAGER=ranger
export AURDEST=/build/aur

# locale
export LANG=en_US.UTF-8
export LC_MONETARY=de_DE.UTF-8
export LC_TIME=de_DE.UTF-8
export TZ=Europe/Berlin

# general
export PAGER=nvimpager
export MANWIDTH=120
export TERMINAL=kitty
export EDITOR=nvim
export VISUAL=$EDITOR
export BROWSER=firefox
export NODE_PATH=/usr/lib/node_modules

# XDG setup
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_DIRS=/usr/share:/usr/local/share
export XDG_CONFIG_DIRS=/etc/xdg


# desktop/wayland
export MOZ_ENABLE_WAYLAND=1
export MOZ_WEBRENDER=1
export MOZ_USE_XINPUT2=1
export CLUTTER_BACKEND=wayland
export QT_QPA_PLATFORM=wayland-egl
export QT_QPA_PLATFORMTHEME=qt5ct
export SDL_VIDEODRIVER=wayland
export SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0
export _JAVA_AWT_WM_NONREPARENTING=1
export NO_AT_BRIDGE=1

export CARGO_HOME=$XDG_DATA_HOME/cargo
export GOPATH=$XDG_DATA_HOME/go
