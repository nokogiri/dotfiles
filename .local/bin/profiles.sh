#!/bin/sh
# easier way to switch sc controller profiles under wayland
# as the transparancy of the osd is not yelping

set_profile() {
    scc set-profile $1
    }

curr_p=`scc info | tail -n1 | awk '{print $4}'|xargs -I % basename % .sccprofile `

set_profile "Desk"

new_p=`scc list-profiles | rofi -dmenu -p "Profiles"`
if [ ! -z $new_p ]; then
    set_profile $new_p
    notify-send --app-name="SC-Controller" "SC-Controller" "${new_p}"
else
    set_profile $curr_p
    notify-send --app-name="SC-Controller" "SC-Controller" "${curr_p}"
fi
